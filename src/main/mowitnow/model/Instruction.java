package mowitnow.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.ToString;
import mowitnow.math.Complex;

@Getter
@ToString
public class Instruction {
	
	private boolean moveForward;
	
	private Complex rotation;
	
	public Instruction(String code) {
		if("A".equals(code)) {
			moveForward = true;
			rotation = null;
		}
		else if ("G".equals(code)){
			moveForward = false;
			rotation=new Complex(0,1);
		}
		else if ("D".equals(code)) {
			moveForward = false;
			rotation = new Complex(0, -1);
		}
		else {
			throw new RuntimeException("Invalid instruction code : " + code);
		}
	}
	
	public static List<Instruction> decodeStringIntructionList(String instructionCodes) {
		List<Instruction> instructions = new ArrayList<>(instructionCodes.length());
		instructionCodes.replaceAll(" ", "");
		for (int i = 0; i<instructionCodes.length(); i++) {
			instructions.add(new Instruction(String.valueOf(instructionCodes.charAt(i))));
		}
		return instructions;
	}
}
