package mowitnow.model;

import lombok.AllArgsConstructor;

/**
 * Classe représentant la pelouse
 * @author jeremy.andriamakaoly
 *
 */
@AllArgsConstructor
public class Lawn {

	int width;
	
	int length;
}
