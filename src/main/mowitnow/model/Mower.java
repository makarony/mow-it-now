package mowitnow.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import mowitnow.math.Complex;

/**
 * Classe représentant une tondeuse
 * @author jeremy.andriamakaoly
 *
 */
@Getter
@Setter
@ToString
public class Mower {
	private static String SEPARATOR = " ";
	
	private Complex position;
	
	private Complex orientation;
	
	private Complex grass;


	public String getStringPosition() {
		return Integer.toString(position.getX()) + SEPARATOR + Integer.toString(position.getY()) + SEPARATOR + orientation.getOrientationCode();
	}
	
}
