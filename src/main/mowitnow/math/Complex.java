package mowitnow.math;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class Complex {
	
	private int x;
	
	private int y;
	
	public static Complex NORTH = new Complex(0, 1);
	
	public static Complex WEST = new Complex(-1, 0);
	
	public static Complex SOUTH = new Complex(0, -1);
	
	public static Complex EAST = new Complex(1, 0);
	
	public static Complex multiply(Complex c1, Complex c2) {
		return new Complex(c1.x*c2.x - c1.y*c2.y, c1.x*c2.y + c1.y*c2.x);
	}
	
	public static Complex add(Complex c1, Complex c2) {
		return new Complex(c1.x + c2.x, c1.y + c2.y);
	}
	
	public String getOrientationCode() {
		if (NORTH.equals(this)) {
			return "N";
		}
		else if (WEST.equals(this)) {
			return "W";
		}
		else if (SOUTH.equals(this)) {
			return "S";
		}
		else if (EAST.equals(this)) {
			return "E";
		}
		else {
			throw new RuntimeException("This complex number is not a orientation " + this);
		}
	}
	
	public static Complex produceFromOrientationCode(String code) {
		if ("N".equals(code)) {
			return Complex.NORTH;
		}
		else if ("W".equals(code)) {
			return Complex.WEST;
		}
		else if ("S".equals(code)) {
			return Complex.SOUTH;
		}
		else if ("E".equals(code)) {
			return Complex.EAST;
		}
		else {
			throw new RuntimeException("Invalid orientation code : " + code);
		}
	}
}
