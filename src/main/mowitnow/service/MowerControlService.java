package mowitnow.service;

import java.util.ArrayList;
import java.util.List;

import mowitnow.input.InputParameters;
import mowitnow.math.Complex;
import mowitnow.model.Instruction;
import mowitnow.model.Mower;

public class MowerControlService {

	public List<Mower> processMowerInstructions(InputParameters parameters) {
		
		List<Mower> finalMowers = new ArrayList<>();
		for (int idxMower = 0; idxMower < parameters.getMowers().size(); idxMower++) {
			for (Instruction instruction : parameters.getMowerInstructions().get(idxMower)) {
				this.moveMower(parameters.getMowers().get(idxMower), instruction, parameters.getGrass());
			}
			System.out.println(parameters.getMowers().get(idxMower).getStringPosition());
			finalMowers.add(parameters.getMowers().get(idxMower));
		}
		
		return finalMowers;
	}

	private void moveMower(Mower mower, Instruction instruction, Complex grass) {
			System.out.println("I receveived instruction : " + instruction);
			if (instruction.isMoveForward()) {
				Complex newPosition = Complex.add(mower.getPosition(), mower.getOrientation());
				if (isPositionOnGrass(newPosition, grass)) {
					mower.setPosition(newPosition);
				}
			}
			if(instruction.getRotation() != null) {
				mower.setOrientation(Complex.multiply(mower.getOrientation(), instruction.getRotation()));
			}
			System.out.println("I moved to : " + mower);
	}
	
	private boolean isPositionOnGrass(Complex position, Complex grass) {
		System.out.println("position : "+position + "grass : "+grass);
		if (position.getX()>=0 && position.getY() >=0 && position.getX()<=grass.getX() && position.getY() <= grass.getY()) {
			return true;
		}
		return false;
	}
}
