package mowitnow;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import mowitnow.input.InputFileReader;
import mowitnow.input.InputParameters;
import mowitnow.model.Mower;
import mowitnow.service.MowerControlService;

public class MowItNowStarter {

	public static void main(final String[] args) {
		System.out.println("Mow it now !");
		for (String arg : args) {
			System.out.println(arg);
		}
		String path = "";
		InputParameters parameters = new InputParameters();

		if (args.length > 0){
			path = args[0];
		} else {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
			fileChooser.setDialogTitle("Mow it now - Select input file");
			if(fileChooser.showOpenDialog(null)==JFileChooser.APPROVE_OPTION) {
				path = fileChooser.getSelectedFile().getPath();
			}
		}
		
		InputFileReader reader = new InputFileReader(path);
		try {
			parameters = reader.getInputParameters(path);
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Error while reading file : " + path, "Mow it Now - ERROR", JOptionPane.ERROR_MESSAGE);
		}
		
		MowerControlService service = new MowerControlService();
		List<Mower> finalMowers = service.processMowerInstructions(parameters);
		
		System.out.println("*** FINAL POSITION OF THE MOWERS ***");
		String finalMowersString = "";
		for (Mower mower : finalMowers) {
			System.out.println(mower.getStringPosition());
			finalMowersString += "\n" + mower.getStringPosition();
		}
		JOptionPane.showMessageDialog(null, finalMowersString, "FINAL POSITION OF THE MOWERS", JOptionPane.INFORMATION_MESSAGE);
		
	}
}
