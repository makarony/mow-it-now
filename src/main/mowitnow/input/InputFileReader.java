package mowitnow.input;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import mowitnow.math.Complex;
import mowitnow.model.Instruction;
import mowitnow.model.Mower;

@RequiredArgsConstructor
public class InputFileReader {

	@NonNull
	private String path;
	
	public InputParameters getInputParameters(String path) throws FileNotFoundException {
		InputParameters parameters = new InputParameters();
		File f = new File(path);
		Scanner scan = new Scanner(f);
		
		//Read Grass config
		Complex grass = new Complex(scan.nextInt(), scan.nextInt());
		System.out.println(grass);
		parameters.setGrass(grass);
		scan.nextLine();
		
		//Read Mowers and associated instructions
		while(true) {
			try {
				String mowerString = scan.nextLine().replaceAll(" ", "");
				System.out.println(mowerString);
				Mower mower = new Mower();
				mower.setPosition(new Complex(Integer.parseInt(mowerString.substring(0,1)), Integer.parseInt(mowerString.substring(1,2))));
				mower.setOrientation(Complex.produceFromOrientationCode(mowerString.substring(2,3)));
				System.out.println(mower);
				parameters.getMowers().add(mower);
				
				String instructionsString = scan.nextLine().replaceAll(" ", "");
				System.out.println(instructionsString);
				List<Instruction> instructions = Instruction.decodeStringIntructionList(instructionsString);
				System.out.println(instructions);
				parameters.getMowerInstructions().add(instructions);
			}
			catch(NoSuchElementException ex) {
				break;
			}
		}
		scan.close();
		
		return parameters;
	}
}
