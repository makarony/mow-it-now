package mowitnow.input;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import mowitnow.math.Complex;
import mowitnow.model.Instruction;
import mowitnow.model.Mower;

@Getter
@Setter
public class InputParameters {

	Complex grass;
	
	List<Mower> mowers = new ArrayList<>();
	
	List<List<Instruction>> mowerInstructions = new ArrayList<>();
	
}
