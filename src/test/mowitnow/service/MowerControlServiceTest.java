package mowitnow.service;

import java.util.List;

import org.junit.Test;

import mowitnow.input.InputParameters;
import mowitnow.math.Complex;
import mowitnow.model.Instruction;
import mowitnow.model.Mower;

public class MowerControlServiceTest {

	MowerControlService service = new MowerControlService();
	
	@Test
	public void testMower1() {
		Mower mower = new Mower();
		mower.setOrientation(Complex.produceFromOrientationCode("N"));
		mower.setPosition(new Complex(1, 2));
		mower.setGrass(new Complex(5, 5));
		List<Instruction> instructions = Instruction.decodeStringIntructionList("GAGAGAGAA");
		InputParameters params = new InputParameters();
		params.setGrass(new Complex(5, 5));
		params.getMowerInstructions().add(instructions);
		params.getMowers().add(mower);
		
		service.processMowerInstructions(params);
	}
	
	@Test
	public void testMower2() {
		Mower mower = new Mower();
		mower.setOrientation(Complex.produceFromOrientationCode("E"));
		mower.setPosition(new Complex(3, 3));
		mower.setGrass(new Complex(5, 5));
		List<Instruction> instructions = Instruction.decodeStringIntructionList("AADAADADDA");
		
		InputParameters params = new InputParameters();
		params.setGrass(new Complex(5, 5));
		params.getMowerInstructions().add(instructions);
		params.getMowers().add(mower);
		service.processMowerInstructions(params);
	}

}
