# Mow It Now

Automatic mowers for rectangular grass

To run the program, download the bin/mowItNowStarter.jar archive, and launch it. You will be asked to enter an input file specifing the grass configuration, initial position of the mowers and instructions for each mower.

You can also specify the input file using the command line : java -jar mowItNowStarter.jar myInputFile.txt

An exemple input file is given in src/main/resources/input.txt